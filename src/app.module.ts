import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MemberModule } from './member/member.module'; // Import modul yang membutuhkan TypeOrmModule
import { BookModule } from './book/book.module'; // Import modul yang membutuhkan TypeOrmModule
import { BorrowModule } from './borrow/borrow.module'; // Import modul yang membutuhkan TypeOrmModule

@Module({
  imports: [
    TypeOrmModule.forRoot({
      // Konfigurasi koneksi database
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'perpus',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    MemberModule, // Import modul yang membutuhkan TypeOrmModule
    BookModule, // Import modul yang membutuhkan TypeOrmModule
    BorrowModule, // Import modul yang membutuhkan TypeOrmModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
