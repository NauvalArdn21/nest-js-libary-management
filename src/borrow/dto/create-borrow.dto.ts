import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class CreateBorrowDto {
  @PrimaryGeneratedColumn()
    id!: number;

  @Column()
    borrowerCode!: string;

  @Column()
    bookCode!: string;

  @Column({ type: 'date' })
    borrowDate!: Date;

  @Column({ type: 'date', nullable: true })
    returnDate!: Date;
}
