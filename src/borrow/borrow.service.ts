import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Borrow } from './borrow.entity';
import { CreateBorrowDto } from './dto/create-borrow.dto';

@Injectable()
export class BorrowService {
  constructor(
    @InjectRepository(Borrow)
    private borrowRepository: Repository<Borrow>,
  ) {}

  async create(createBorrowDto: CreateBorrowDto): Promise<Borrow> {
    const newBorrow = this.borrowRepository.create(createBorrowDto);
    return await this.borrowRepository.save(newBorrow);
  }

  async findAll(): Promise<Borrow[]> {
    return await this.borrowRepository.find();
  }
}
