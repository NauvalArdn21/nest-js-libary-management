import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Borrow } from './borrow.entity';
import { BorrowController } from './borrow.controller';
import { BorrowService } from './borrow.service';

@Module({
 imports: [TypeOrmModule.forFeature([Borrow])],
  controllers: [BorrowController],
  providers: [BorrowService],
})
export class BorrowModule {}
