// src/borrow/borrow.entity.ts

import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column } from 'typeorm';
import { Member } from '../member/member.entity';
import { Book} from '../book/book.entity';

@Entity()
export class Borrow {
  @PrimaryGeneratedColumn()
    id!: number;

  @ManyToOne(() => Member, { eager: true })
    @JoinColumn({ name: 'memberId' })
    member!: Member;

  @ManyToOne(() => Book, { eager: true })
  @JoinColumn({ name: 'bookId' })
  book: Book | undefined;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    borrowedDate!: Date;

  @Column({ type: 'timestamp', nullable: true })
    returnedDate!: Date;

  @Column({ default: false })
    isReturned!: boolean;
}
