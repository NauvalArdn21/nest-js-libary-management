// src/book/book.service.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from './book.entity';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private readonly bookRepository: Repository<Book>,
  ) {}

  async findAll(): Promise<Book[]> {
    return this.bookRepository.find();
  }

  // src/book/book.service.ts

async findByCode(code: string): Promise<Book | null> {
    const book = await this.bookRepository.findOne({ where: { code } });
    return book || null; // Handle case when book is not found
  }
  

  async create(book: Book): Promise<Book> {
    const newBook = this.bookRepository.create(book);
    return this.bookRepository.save(newBook);
  }

  async update(code: string, updatedBook: Partial<Book>): Promise<Book> {
    const book = await this.findByCode(code);
    if (!book) {
      throw new Error(`Book with code ${code} not found`);
    }
    Object.assign(book, updatedBook);
    return this.bookRepository.save(book);
  }

  async delete(code: string): Promise<void> {
    const book = await this.findByCode(code);
    if (!book) {
      throw new Error(`Book with code ${code} not found`);
    }
    await this.bookRepository.remove(book);
  }
}
