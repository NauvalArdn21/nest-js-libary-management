// src/book/book.controller.ts

import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { BookService } from './book.service';
import { Book } from './book.entity';

@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get()
  async findAll(): Promise<Book[]> {
    return this.bookService.findAll();
  }

  

  @Post()
  async create(@Body() book: Book): Promise<Book> {
    return this.bookService.create(book);
  }

  @Put(':code')
  async update(@Param('code') code: string, @Body() updatedBook: Partial<Book>): Promise<Book> {
    return this.bookService.update(code, updatedBook);
  }

  @Delete(':code')
  async delete(@Param('code') code: string): Promise<void> {
    return this.bookService.delete(code);
  }
}
