// src/member/member.service.ts

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Member } from './member.entity';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private readonly memberRepository: Repository<Member>,
  ) {}

  async findAll(): Promise<Member[]> {
    return await this.memberRepository.find();
  }

  async findById(code: string): Promise<Member> {
    const member = await this.memberRepository.findOne({ where: { code } });
    if (!member) {
      throw new NotFoundException(`Member with code '${code}' not found`);
    }
    return member;
  }

  async create(createMemberDto: CreateMemberDto): Promise<Member> {
    const { code, name } = createMemberDto;
    const newMember = this.memberRepository.create({ code, name });
    return await this.memberRepository.save(newMember);
  }

  async update(code: string, updateMemberDto: UpdateMemberDto): Promise<Member> {
    const { name } = updateMemberDto;
    const member = await this.findById(code);
    member.name = name;
    return await this.memberRepository.save(member);
  }

  async delete(code: string): Promise<void> {
    const member = await this.findById(code);
    await this.memberRepository.remove(member);
  }

  async addPenaltyPoints(code: string, pointsToAdd: number): Promise<Member> {
    const member = await this.findById(code);
    member.penaltyPoints += pointsToAdd;
    return await this.memberRepository.save(member);
  }

  async reducePenaltyPoints(code: string, pointsToReduce: number): Promise<Member> {
    const member = await this.findById(code);
    member.penaltyPoints -= pointsToReduce;
    if (member.penaltyPoints < 0) {
      member.penaltyPoints = 0;
    }
    return await this.memberRepository.save(member);
  }
}
