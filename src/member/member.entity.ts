// src/member/member.entity.ts

import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  code: string | undefined;

  @Column({ type: 'varchar', length: 255 }) // Sesuaikan panjang dan tipe data sesuai kebutuhan
  name: string | undefined;

  @Column({ default: 0 }) // Menambahkan kolom penaltyPoints dengan nilai default 0
  penaltyPoints!: number; // Non-null assertion operator
}
