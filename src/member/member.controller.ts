// src/member/member.controller.ts

import { Controller, Get, Post, Put, Delete, Param, Body, Patch } from '@nestjs/common';
import { MemberService } from './member.service';
import { Member } from './member.entity';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';

@Controller('members')
export class MemberController {
  constructor(private readonly memberService: MemberService) {}

  @Get()
  async findAll(): Promise<Member[]> {
    return this.memberService.findAll();
  }

  @Get(':code')
  async findOne(@Param('code') code: string): Promise<Member> {
    return this.memberService.findById(code);
  }

  @Post()
  async create(@Body() createMemberDto: CreateMemberDto): Promise<Member> {
    return this.memberService.create(createMemberDto);
  }

  @Put(':code')
  async update(@Param('code') code: string, @Body() updateMemberDto: UpdateMemberDto): Promise<Member> {
    return this.memberService.update(code, updateMemberDto);
  }

  @Delete(':code')
  async delete(@Param('code') code: string): Promise<void> {
    return this.memberService.delete(code);
  }

  @Patch(':code/add-penalty/:points')
  async addPenaltyPoints(@Param('code') code: string, @Param('points') points: number): Promise<Member> {
    return this.memberService.addPenaltyPoints(code, points);
  }

  @Patch(':code/reduce-penalty/:points')
  async reducePenaltyPoints(@Param('code') code: string, @Param('points') points: number): Promise<Member> {
    return this.memberService.reducePenaltyPoints(code, points);
  }
}
